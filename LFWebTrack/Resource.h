//{{NO_DEPENDENCIES}}
// Von Microsoft Visual C++ generierte Includedatei.
// Verwendet durch LFWebTrack.rc
//
#define IDOK2                           2
#define IDABORT                         3
#define IDC_MYICON                      4
#define IDD_LFWEBTRACK_DIALOG           102
#define IDS_APP_TITLE                   103
#define IDM_ABOUT                       104
#define IDM_EXIT                        105
#define IDC_LFWEBTRACK                  109
#define IDR_MAINFRAME                   128
#define IDD_MainDia                     130
#define IDD_ContentUpdated              131
#define IDI_LFWEBTRACK                  132
#define IDI_LFWEBTRACK1                 133
#define IDI_SMALL                       133
#define IDC_EDIT1                       1000
#define IDC_EDIT2                       1001
#define IDC_UpdateText                  -5
#define IDC_STATICBLOB                  -4
#define IDC_STATIC2                     -3
#define IDC_STATIC1                     -2
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        133
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           110
#endif
#endif
