#pragma once

#include "resource.h"
#include <curl.h>
#define CURL_STATICLIB
//#pragma comment (lib, "libcurl_a_debug.lib")
#include <openssl/md5.h>
#pragma comment (lib, "libeay32MDd.lib")
#pragma comment (lib, "libcurl_a.lib")

struct MemoryStruct {
	char *memory;
	size_t size;
};

HWND hWnd;
HINSTANCE hInst;                                // Aktuelle Instanz
char tbURL[256];
char tbTime[64];
int tbiTime = 0;
bool running = false;
bool firstFetch = false;
CURL* curl;
CURLcode res;
unsigned char* md5out1, * md5out2;

void setStaticTextFont(HWND hDlg, int size, LPCSTR fontName, int staticElementID);
void getWebUpdate();
void updateGOButton(HWND hDlg);

static size_t handleCurlData(void *buffer, size_t size, size_t nmemb, void *userp);
