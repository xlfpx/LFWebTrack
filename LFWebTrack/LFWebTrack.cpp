// LFWebTrack.cpp : Definiert den Einstiegspunkt für die Anwendung.
//

#include "stdafx.h"
#include "LFWebTrack.h"

#define MAX_LOADSTRING 100

// Globale Variablen:
WCHAR szTitle[MAX_LOADSTRING];                  // Titelleistentext
WCHAR szWindowClass[MAX_LOADSTRING];            // Klassenname des Hauptfensters

// Vorwärtsdeklarationen der in diesem Codemodul enthaltenen Funktionen:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    MainDiaProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	ContentUpdatedProc(HWND, UINT, WPARAM, LPARAM);
char curl_errbuf[CURL_ERROR_SIZE];
char output[2048];

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

	// Initialize curl
	curl = curl_easy_init();
	if (!curl)
		DebugBreak();
	md5out1 = (unsigned char*)malloc(MD5_DIGEST_LENGTH);
	md5out2 = (unsigned char*)malloc(MD5_DIGEST_LENGTH);

    // Globale Zeichenfolgen initialisieren
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_LFWEBTRACK, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);


    // Anwendungsinitialisierung ausführen:
    if (!InitInstance (hInstance, nCmdShow))
		return FALSE;
    

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_LFWEBTRACK));
    MSG msg;

    // Hauptnachrichtenschleife:
	for (;;) {
	
		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) {

			TranslateMessage(&msg);
			DispatchMessage(&msg);

		}

		if (msg.message == WM_QUIT)
			break;

		if (running) {
			getWebUpdate();
			Sleep(tbiTime);
		}

	}
   
	// curl cleanup
	curl_easy_cleanup(curl);
	free(md5out1);
	free(md5out2);
    return (int) msg.wParam;

}



//
//  FUNKTION: MyRegisterClass()
//
//  ZWECK: Registriert die Fensterklasse.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_LFWEBTRACK));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_LFWEBTRACK);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}


BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Instanzenhandle in der globalen Variablen speichern

   /*
   // DISABLED WINDOW BECAUSE NO NEED
   hWnd = CreateWindowW(szWindowClass, szTitle, WS_POPUP | WS_BORDER,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, SW_SHOW);
   UpdateWindow(hWnd);
   */
   DialogBox(hInst, MAKEINTRESOURCE(IDD_MainDia), NULL, MainDiaProc);
   
   return TRUE;

}


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

INT_PTR CALLBACK MainDiaProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
	
    switch (message)
    {
    case WM_INITDIALOG:
		setStaticTextFont(hDlg, 250, "Arial", IDC_STATIC1);
		setStaticTextFont(hDlg, 170, "Arial", IDC_STATIC2);
		setStaticTextFont(hDlg, 90, "Arial", IDC_STATICBLOB);
		CWnd::FromHandle(GetDlgItem(hDlg, IDOK2))->EnableWindow(false);
        return (INT_PTR)TRUE;
		break;

    case WM_COMMAND:

		if (LOWORD(wParam) == IDABORT) {

			EndDialog(hDlg, LOWORD(wParam));
			PostQuitMessage(NULL);
			return (INT_PTR)TRUE;

		} else if (LOWORD(wParam) == IDOK2){
			
			curl_easy_setopt(curl, CURLOPT_URL, tbURL); //  e.g.: http://coinmarketcap.northpole.ro/api/btc.json
			curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, curl_errbuf);
			curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, handleCurlData);
			curl_easy_setopt(curl, CURLOPT_USERAGENT, "libcurl-agent/1.0");
			tbiTime = atoi(tbTime);
			running = true;
			firstFetch = true;
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;

		}else if(LOWORD(wParam) == IDC_EDIT1){
		
			if (HIWORD(wParam) == EN_UPDATE) {

				CWnd::FromHandle(GetDlgItem(hDlg, IDC_EDIT1))->GetWindowTextA(tbURL, 256);
				updateGOButton(hDlg);

			}
		
		}else if (LOWORD(wParam) == IDC_EDIT2) {

			if (HIWORD(wParam) == EN_UPDATE) {

				CWnd::FromHandle(GetDlgItem(hDlg, IDC_EDIT2))->GetWindowTextA(tbTime, 256);
				updateGOButton(hDlg);

			}

		}
        break;

    }
    return (INT_PTR)FALSE;
}

INT_PTR CALLBACK ContentUpdatedProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	char* message_text;
	char cbuffer[32];
	switch (message)
	{
	case WM_INITDIALOG:
		sprintf(cbuffer, "%.1f", (float)tbiTime / 1000.0f);
		message_text = (char*)malloc(256);
		ZeroMemory(message_text, 256);
		strcpy(message_text, "The Web-Content at \"");
		strcat(message_text, tbURL);
		strcat(message_text, "\" you're currently tracking has changed since it has last been checked! (checking-interval of ");
		strcat(message_text, cbuffer);
		strcat(message_text, " seconds)\n\nDo you want to continue tracking it?");
		
		setStaticTextFont(hDlg, 170, "Arial", IDC_UpdateText);
		CWnd::FromHandle(GetDlgItem(hDlg, IDC_UpdateText))->SetWindowTextA(message_text);
		return (INT_PTR)TRUE;
		break;

	case WM_COMMAND:

		if (LOWORD(wParam) == IDABORT || LOWORD(wParam) == IDNO) {

			EndDialog(hDlg, LOWORD(wParam));
			PostQuitMessage(NULL);
			return (INT_PTR)TRUE;

		}
		else if (LOWORD(wParam) == IDYES) {

			running = true;
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;

		}
		break;

	}
	return (INT_PTR)FALSE;
}

void setStaticTextFont(HWND hDlg, int size, LPCSTR fontName, int staticElementID) {

	CFont *m_Font1 = new CFont;
	if (!m_Font1->CreatePointFont(size, fontName))
		DebugBreak();
	
	CWnd::FromHandle(GetDlgItem(hDlg, staticElementID))->SetFont(m_Font1);

}

void getWebUpdate() {

	struct MemoryStruct memstruct;
	memstruct.memory = (char*)malloc(1);  // will be grown as needed by the realloc above
	memstruct.size = 0;
	memstruct.memory[0] = NULL;
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &memstruct);

	if ((res = curl_easy_perform(curl)) != CURLE_OK) {

		MessageBox(NULL, curl_errbuf, "Curl Error", MB_OK);
		MessageBox(NULL, curl_easy_strerror(res), "Curl Error", MB_OK);
		PostQuitMessage(NULL);

	}
	else if(!firstFetch){

		MD5((unsigned char*)memstruct.memory, memstruct.size, md5out1);
		if (memcmp(md5out1, md5out2, MD5_DIGEST_LENGTH) != 0) {
		
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ContentUpdated), NULL, ContentUpdatedProc);
			memcpy(md5out2, md5out1, MD5_DIGEST_LENGTH);

		}

	}
	else {
	
		MD5((unsigned char*)memstruct.memory, memstruct.size, md5out1);
		memcpy(md5out2, md5out1, MD5_DIGEST_LENGTH);
		firstFetch = false;

	}

	free(memstruct.memory);

}

static size_t handleCurlData(void *buffer, size_t size, size_t nmemb, void *userp) {

	size_t realsize = size * nmemb;
	struct MemoryStruct *mem = (struct MemoryStruct *)userp;

	mem->memory = (char*) realloc(mem->memory, mem->size + realsize + 1);
	if (mem->memory == NULL) {
		// out of memory!
		MessageBox(NULL, "not enough memory (realloc returned NULL)\n", "Out of Memory!", MB_OK);
		return 0;
	}

	memcpy(&(mem->memory[mem->size]), buffer, realsize);
	mem->size += realsize;
	mem->memory[mem->size] = 0;

	return realsize;

}

void updateGOButton(HWND hDlg) {

	CWnd::FromHandle(GetDlgItem(hDlg, IDOK2))->EnableWindow((strlen(tbURL) != 0 && strlen(tbTime) != 0));

}
